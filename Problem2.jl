### A Pluto.jl notebook ###
# v0.14.1

using Markdown
using InteractiveUtils

# ╔═╡ 7323b12b-8f36-40d3-98e2-66eb87d2de49
import Pkg; 

# ╔═╡ 735e51b9-9400-4fd1-998e-be1dda54f3a2
Pkg.add("PlutoUI")

# ╔═╡ 26bc3e25-7551-42a9-9586-0d386b8d763d
using PlutoUI

# ╔═╡ 007d822b-47cd-4667-85e3-290737731347
function evaluate(maxmizingP)
	 if maxmizingP == true
		return MaxPlayer
	else
		return MinPlayer
	end
end


# ╔═╡ e19c60c9-a011-40c8-8672-7e0494af14ae
function minimax(position, depth , maximizingP)
	if depth =0
		return none, evaluate(maxmizingP)
		moves = position.get moves()
		best_move = random.choice(moves)

	if maximizingP
		max eval=-inf
		for move in moves
			position.make move(move[0], move[1])
			current eval = minimax(position, depth -1 ,alpha,beta, false  ,maximizingP)[1]
			position.unmake move()
				if current_eval > max eval
					max eval =current_eval
					best_move = move
					return best_move , max_eval
				else
					min eval=inf
		for move in moves
			position.make move(move[0], move[1])
			current eval = minimax(position, depth -1 ,alpha,beta, true  ,maximizingP)[1]
			position.unmake move()
				if current_eval > min eval
					min eval =current_eval
					best_move = move
					return best_move , min_eval
						end
				end
			end
		end
	end
end
			
			

# ╔═╡ Cell order:
# ╟─7323b12b-8f36-40d3-98e2-66eb87d2de49
# ╠═735e51b9-9400-4fd1-998e-be1dda54f3a2
# ╠═26bc3e25-7551-42a9-9586-0d386b8d763d
# ╠═007d822b-47cd-4667-85e3-290737731347
# ╠═e19c60c9-a011-40c8-8672-7e0494af14ae
