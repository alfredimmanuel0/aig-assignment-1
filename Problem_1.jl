### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ be31a1c8-69d9-4b5e-ad18-3db31f85e2ae
using Pkg

# ╔═╡ 294bc107-2be8-4e48-a03b-02d2b958fc40
Pkg.activate("Project.toml")

# ╔═╡ 0e37dece-aea3-4654-83c4-96aac4b8649a
Pkg.add("DataStructures")

# ╔═╡ 351d7d12-3cba-46e5-b88e-975b3ed66735
using PlutoUI

# ╔═╡ f53b0c57-f37a-43f4-ac94-624640cdbac3
using DataStructures

# ╔═╡ d8084cb0-abf2-11eb-066e-034bb027a776
md"# Assignment 1"

# ╔═╡ 83cb6f21-2791-41c4-bf20-170346cdd97d
md"## Problem 1"

# ╔═╡ fee7f249-47e3-4ff6-8440-df67c5a83e41
md"###### Alpha and Queen"

# ╔═╡ 3bc20fa7-30c7-4116-8ed5-e9e59997b16e
struct Action
	Name::String
	cost::Int64
end

# ╔═╡ 2a2a88dc-d1d1-4a78-ae8e-64db1878feff
re = Action("Remain",1)

# ╔═╡ 7e7a662e-3b06-4270-bed7-67f23cafbfee
me = Action("Move East",3)

# ╔═╡ 395c7d33-7c25-4e04-896d-c7b987860668
mw = Action("Move West",3)

# ╔═╡ ee4a84a1-d639-4bcf-bab4-49544fa5861c
co = Action("Collect",5)

# ╔═╡ 55c4d97f-e247-4343-b9b4-4a589f3ee27a
struct State
	Name::String 
	hasItems::Vector{Bool}
	Position::Int64
end

# ╔═╡ 6d3ce307-b6ed-4261-b368-85950ed0cdac
ZeRo = State("ZeRo State", [true],2)

# ╔═╡ b5588cf4-f5c4-49ad-8a11-aaed62a460a3
OnE = State("OnE State",[true],3)

# ╔═╡ 016b4a36-e672-42be-bc2b-9245a081157d
TwO =State("TwO Sate",[true],4)

# ╔═╡ 82f974f1-b749-4318-b89b-908d7c37d804
ThRee= State("ThRee State",[true],3)

# ╔═╡ 109da344-ee8e-4b0d-83a5-f69272f06804
FoUr = State("FoUr State",[true],2)

# ╔═╡ ede217f5-e5fe-4588-a0e9-31493d7ab864
FiVe = State("FiVe State",[true],1)

# ╔═╡ 9f4ec7f8-6567-498e-aeef-550a2082a196
SiX = State("SiX State",[true],2)

# ╔═╡ 7fca5598-434b-4dfd-9dd6-36d81f1be5a0
SeVeN = State("SeVeN State",[true],3)

# ╔═╡ e2d4d744-6e0a-4f67-9c9b-ff3081596930
EiGhT = State("EiGhT State",[false],4)

# ╔═╡ 7f98cefa-6a3e-40b0-a6d9-0e78e06d8f26
NiNe = State("NiNe State",[false],3)

# ╔═╡ f6feb278-62d0-4f57-8c0d-242d0e370366
TeN = State("TeN State",[false],2)

# ╔═╡ 6b00c8f2-f263-44e9-ab1d-dee0f73abdf3
ElEvEn = State("ElEvEn State",[false],1)

# ╔═╡ ef10df3b-595b-4321-9af7-577a87492705
TwElVe = State("TwElVe State",[false],2)

# ╔═╡ 36b3fdac-0b92-469c-ad7b-3fabd133d01e
ThIrTeeN =State("ThIrTeeN State",[false],3)

# ╔═╡ f9c7baf3-2d52-4668-a8c5-0f36d46a5399
FoUrTeeN = State("FoUrteeN State",[false],4)

# ╔═╡ b910796a-727f-4f79-aa4f-004298c1147b
goalstates = [FoUrTeeN]

# ╔═╡ 95548162-e13a-4c76-9f93-02a6962cc793
TransitionModel = Dict(ZeRo => [(me,OnE),(co,ZeRo)])

# ╔═╡ 1acfeb9c-5965-404d-80b2-2d9e66661e05
push!(TransitionModel , OnE => [(co,ZeRo),(me,TwO)])

# ╔═╡ e1cb9687-998a-49ef-a0a7-e5f12b05d357
push!(TransitionModel , TwO => [(co,TwO),(re,OnE),(mw,ThRee)])

# ╔═╡ d5945083-1bd7-4e79-b0b1-81757d6782a2
push!(TransitionModel , ThRee => [(co,ThRee),(mw,TwO)])

# ╔═╡ 1723eb39-dc03-4e8a-a7cb-0e349829162a
push!(TransitionModel , FoUr => [(co,FoUr),(mw,FiVe)])

# ╔═╡ 82011f7f-dce5-4d11-b063-4db07ea71e91
push!(TransitionModel , FiVe => [(co,FiVe),(re,FiVe),(me,SiX)])

# ╔═╡ bbd6154b-ef0b-4185-bd43-fed7ef445beb
push!(TransitionModel , SiX => [(co,SiX),(mw,SeVeN)])

# ╔═╡ 91e213e2-9851-4536-b6ae-f5311b298bcf
push!(TransitionModel , SeVeN => [(me,EiGhT)])

# ╔═╡ adc54c15-fe48-4b08-9a13-297fb4898e8b
push!(TransitionModel , EiGhT => [(re,EiGhT),(mw,NiNe)])

# ╔═╡ 72df2d84-9061-4677-8464-a86db386b69b
push!(TransitionModel , NiNe => [(mw,TeN)])

# ╔═╡ 9125c083-6fe4-41f5-899a-ee746acd4182
push!(TransitionModel , TeN => [(mw,ElEvEn)])

# ╔═╡ 0e17a0c0-40a7-4e1f-b816-ecc920974347
push!(TransitionModel , ElEvEn => [(re,ElEvEn),(me,TwElVe)])

# ╔═╡ 6a1f2579-4777-44e6-8ba3-6b95b1d4852c
push!(TransitionModel , TwElVe => [(me,ThIrTeeN)])

# ╔═╡ 97a1f965-f6cd-48dc-b7c4-8b32e01c7ad2
push!(TransitionModel , ThIrTeeN => [(me,FoUrTeeN)])

# ╔═╡ a0f50e19-573d-4812-a12d-4ac9477472c1
push!(TransitionModel , FoUrTeeN => [(re,FoUrTeeN)])

# ╔═╡ 8c52d261-624a-4c4c-8ce4-3509ee19ef6e
TransitionModel[ZeRo]

# ╔═╡ 893411d2-b5a1-4ce1-a0bf-6fa69b0a3857
function A_Star(ZeRo, TransitionModel, goalstates,clean,removeDirt)
	explored = []
	beginState = Tmodel{State, State}()
	initial = ZeRo
	
	while true
		if isempty(office)
			return []
		else
			(dirt, noDirt) = clean(office)
			current_state = dirt
			office = noDirt
			
			push!(explored, current_state)
			office_Part = TransitionModel[current_state]
			for oneOffice in office_Part
				if !((oneOffice[2]+cost) in explored)
					push!(beginState, oneOffice[2]+cost => current_state)
					if (goalstate(oneOffice[2]+cost))
						return create_result(TransitionModel, beginState,
						ZeRo, oneOffice[2]+cost)
					else
						office_Part = removeDirt(office_Part,
						oneOffice[2]+cost, oneOffice[1].cost)
					end
				end
			end
		end
	end
end
		
	
	

# ╔═╡ b33cd619-690d-447d-ac96-1a6af7d201bd

	

# ╔═╡ Cell order:
# ╟─d8084cb0-abf2-11eb-066e-034bb027a776
# ╠═83cb6f21-2791-41c4-bf20-170346cdd97d
# ╠═fee7f249-47e3-4ff6-8440-df67c5a83e41
# ╠═be31a1c8-69d9-4b5e-ad18-3db31f85e2ae
# ╠═294bc107-2be8-4e48-a03b-02d2b958fc40
# ╠═351d7d12-3cba-46e5-b88e-975b3ed66735
# ╠═3bc20fa7-30c7-4116-8ed5-e9e59997b16e
# ╠═2a2a88dc-d1d1-4a78-ae8e-64db1878feff
# ╠═7e7a662e-3b06-4270-bed7-67f23cafbfee
# ╠═395c7d33-7c25-4e04-896d-c7b987860668
# ╠═ee4a84a1-d639-4bcf-bab4-49544fa5861c
# ╠═55c4d97f-e247-4343-b9b4-4a589f3ee27a
# ╠═6d3ce307-b6ed-4261-b368-85950ed0cdac
# ╠═b5588cf4-f5c4-49ad-8a11-aaed62a460a3
# ╠═016b4a36-e672-42be-bc2b-9245a081157d
# ╠═82f974f1-b749-4318-b89b-908d7c37d804
# ╠═109da344-ee8e-4b0d-83a5-f69272f06804
# ╠═ede217f5-e5fe-4588-a0e9-31493d7ab864
# ╠═9f4ec7f8-6567-498e-aeef-550a2082a196
# ╠═7fca5598-434b-4dfd-9dd6-36d81f1be5a0
# ╠═e2d4d744-6e0a-4f67-9c9b-ff3081596930
# ╠═7f98cefa-6a3e-40b0-a6d9-0e78e06d8f26
# ╠═f6feb278-62d0-4f57-8c0d-242d0e370366
# ╠═6b00c8f2-f263-44e9-ab1d-dee0f73abdf3
# ╠═ef10df3b-595b-4321-9af7-577a87492705
# ╠═36b3fdac-0b92-469c-ad7b-3fabd133d01e
# ╠═f9c7baf3-2d52-4668-a8c5-0f36d46a5399
# ╠═b910796a-727f-4f79-aa4f-004298c1147b
# ╠═95548162-e13a-4c76-9f93-02a6962cc793
# ╠═1acfeb9c-5965-404d-80b2-2d9e66661e05
# ╠═e1cb9687-998a-49ef-a0a7-e5f12b05d357
# ╠═d5945083-1bd7-4e79-b0b1-81757d6782a2
# ╠═1723eb39-dc03-4e8a-a7cb-0e349829162a
# ╠═82011f7f-dce5-4d11-b063-4db07ea71e91
# ╠═bbd6154b-ef0b-4185-bd43-fed7ef445beb
# ╠═91e213e2-9851-4536-b6ae-f5311b298bcf
# ╠═adc54c15-fe48-4b08-9a13-297fb4898e8b
# ╠═72df2d84-9061-4677-8464-a86db386b69b
# ╠═9125c083-6fe4-41f5-899a-ee746acd4182
# ╠═0e17a0c0-40a7-4e1f-b816-ecc920974347
# ╠═6a1f2579-4777-44e6-8ba3-6b95b1d4852c
# ╠═97a1f965-f6cd-48dc-b7c4-8b32e01c7ad2
# ╠═a0f50e19-573d-4812-a12d-4ac9477472c1
# ╠═8c52d261-624a-4c4c-8ce4-3509ee19ef6e
# ╠═0e37dece-aea3-4654-83c4-96aac4b8649a
# ╠═f53b0c57-f37a-43f4-ac94-624640cdbac3
# ╠═893411d2-b5a1-4ce1-a0bf-6fa69b0a3857
# ╠═b33cd619-690d-447d-ac96-1a6af7d201bd
